import createRedux from '../utils/createRedux';

const INITIAL_STATE = {};

const selectedItemChange = (state, { item }) => item;

const { actions, reducer, types: selectedItemTypes } = createRedux(INITIAL_STATE, { selectedItemChange });

export default actions;
export { reducer, selectedItemTypes };
