import { combineReducers } from 'redux';

// reducers
import { reducer as selectedItem } from './selectedItemReducer';

export default combineReducers({ selectedItem });
