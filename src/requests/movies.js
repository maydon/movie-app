export async function getListMovies() {
  try {
    const response = await fetch('https://r0ro.fr/tmp/movies.json');
    if (response.status === 200) {
      const result = await response.json();
      return { status: 'OK', data: result };
    } else {
      const result = await response.json();
      return { status: 'ERROR', error: result };
    }
  } catch (e) {
    return { status: 'ERROR', error: e };
  }
}
