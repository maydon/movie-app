import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';

// containers
import MoviesListContainer from '../containers/MoviesListContainer';
import MovieDetails from '../containers/MovieDetailsContainer';

const AppNavigator = () =>
  createStackNavigator(
    {
      moviesList: {
        screen: MoviesListContainer,
        navigationOptions: {
          title: 'Movies',
        },
      },
      movieDetails: {
        screen: MovieDetails,
      },
    },
    {
      initialRouteName: 'moviesList',
      headerMode: 'screen',
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  );

export default createAppContainer(AppNavigator());
