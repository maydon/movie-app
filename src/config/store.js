import { createStore, applyMiddleware } from 'redux';

// middlewares
/* import createSagaMiddleware from 'redux-saga'; */
import logger from 'redux-logger';

// reducers and saga
import reducers from '../reducers';
/* import sagas from '../sagas'; */

/* const sagaMiddleware = createSagaMiddleware(); */

/* const middlewares = [sagaMiddleware]; */

/* if (__DEV__) {
  middlewares.push(logger);
} */

const store = createStore(reducers);

/* sagaMiddleware.run(sagas); */

export default store;
