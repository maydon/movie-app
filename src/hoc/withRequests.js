import React, { useReducer } from 'react';
import { pickBy } from 'lodash';

import createRedux from '../utils/createRedux';

function useRequestState(fn) {
  const INITIAL_STATE = { fetching: false, error: '', data: {} };

  const fetching = state => ({ ...state, fetching: true, error: '' });
  const success = (state, { data }) => ({ ...state, data, fetching: false });
  const failure = (state, { error }) => ({ ...state, error, fetching: false });

  const { actions, reducer } = createRedux(INITIAL_STATE, { fetching, success, failure });

  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

  async function call(...params) {
    try {
      const apiParams = params.map(param => {
        if (typeof param === 'object') {
          return pickBy(param, val => val !== undefined);
        }
        return param;
      });

      await dispatch(actions.fetching());

      const response = await fn(...apiParams);
      if (response.status === 'OK' && response.data) {
        await dispatch(actions.success({ data: response.data }));
      } else {
        await dispatch(actions.failure({ error: response.error }));
      }
    } catch (e) {
      await dispatch(
        actions.failure({
          error: "Erreur inconnue, vérifiez votre connexion Internet ou essayez d'actualiser la page.",
        }),
      );
    }
  }

  return { ...state, call };
}

function withApis(requests) {
  return function(WrappedComponent) {
    return props => {
      const injectedProps = {};
      const keys = Object.keys(requests);
      const length = keys.length;

      for (let i = 0; i < length; i += 1) {
        const key = keys[i];
        injectedProps[key] = useRequestState(requests[key]);
      }
      return <WrappedComponent {...injectedProps} {...props} />;
    };
  };
}

export default withApis;
