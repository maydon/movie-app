import React from 'react';
import { View, Image, Animated, Dimensions } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles/movieDetails';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

const { width } = Dimensions.get('screen');

class MovieDetails extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title'),
  });

  state = {
    paused: true,
  };

  progress = new Animated.Value(0);

  constructor(props) {
    super(props);
    props.navigation.setParams({ title: this.props.selectedItem.page.movie_title });
    this.playDuration = this.getSeconds(this.props.selectedItem.clips[0].runtime);
  }

  getSeconds = time => {
    // create array where first row is minutes and second row is seconds
    const timeArray = time.split(':').map(t => t - 0);
    return timeArray[0] * 60 + timeArray[1];
  };

  togglePlay = () => {
    this.setState(state => ({ paused: !state.paused }));
  };

  onProgress = ({ currentTime }) => {
    const percent = (currentTime * 100) / this.playDuration;

    Animated.timing(this.progress, {
      toValue: (percent * (width - 40)) / 100,
      duration: 100,
    }).start();
  };

  render() {
    const { selectedItem } = this.props;
    const clip = selectedItem.clips[0];
    return (
      <View style={styles.container}>
        <ScrollView>
          <Video onProgress={this.onProgress} paused={this.state.paused} source={{ uri: clip.versions.enus.sizes.sd.src }} style={styles.video} />
          <Animated.View style={{ width: this.progress, backgroundColor: 'red', height: 5 }} />
          <View style={styles.icons}>
            <TouchableOpacity onPress={this.togglePlay}>
              <Icon name={this.state.paused ? 'ios-play' : 'ios-pause'} size={24} />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  selectedItem: state.selectedItem,
});

export default connect(mapStateToProps)(MovieDetails);
