import React, { Component } from 'react';
import { StatusBar, StyleSheet, SafeAreaView } from 'react-native';

import Navigation from '../navigation';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

class RootContainer extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar />
        <Navigation />
      </SafeAreaView>
    );
  }
}

export default RootContainer;
