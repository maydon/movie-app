import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('screen');

const PADDING_HORIZONTAL = 20;
const PADDING_VERTICAL = 10;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: PADDING_HORIZONTAL,
    paddingVertical: PADDING_VERTICAL,
  },
  itemImage: {
    width: width - PADDING_HORIZONTAL * 2,
    height: width - PADDING_HORIZONTAL * 2,
    borderRadius: 4,
  },
  itemTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    paddingBottom: PADDING_VERTICAL,
  },
  subtitile:{
    fontSize: 14,
    textAlign: 'left',
    paddingBottom: PADDING_VERTICAL,
    color:"gray"
  },
  itemFooter: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: PADDING_VERTICAL,
  },
  itemFooterText: {
    fontSize: 12,
  },
  itemIconContainer: {
    paddingLeft: PADDING_HORIZONTAL / 4,
  },
});
