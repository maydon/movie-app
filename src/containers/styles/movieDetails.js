import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

const PADDING_HORIZONTAL = 20;
const PADDING_VERTICAL = 10;

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  video: {
    width: width - 40,
    height: width - 40,
    backgroundColor: 'black',
  },
  icons: {
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    paddingHorizontal: 10,
  },
});
