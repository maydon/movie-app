import React from 'react';
import { View, Text, Image } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { Header } from 'react-navigation';
import { isArray } from 'lodash';
import Icon from 'react-native-vector-icons/Ionicons';

import withRequests from '../hoc/withRequests';
import { getListMovies } from '../requests';
import selectedItemActions from '../reducers/selectedItemReducer';

import styles from './styles/moviesList';

class MoviesList extends React.Component {
  componentDidMount() {
    this.props.list.call();
  }

  flattenData = data => {
    if (!isArray(data)) return [];
    let result = [];
    data.forEach(item => {
      if (!isArray(item)) result.push(item);
      else {
        result = [...result, ...this.flattenData(item)];
      }
    });
    return result;
  };

  renderItem = ({ item }) => {
    const onPress = () => {
      this.props.selectedItemChange(item);
      this.props.navigation.navigate('movieDetails');
    };

    return (
      <TouchableOpacity onPress={onPress} activeOpacity={1} style={styles.itemContainer}>
        <Text style={styles.itemTitle}>{item.page.movie_title}</Text>
        <Text style={styles.subtitile}>{item.details.genres[0].name}</Text>
        <Image resizeMode={'stretch'} style={styles.itemImage} source={{ uri: item.heros[0].imageurl }} />
        <View style={styles.itemFooter}>
          <Text style={styles.itemFooterText}>View Trailer</Text>
          <View style={styles.itemIconContainer}>
            <Icon name="ios-arrow-forward" size={10} />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const data = this.flattenData(this.props.list.data);

    return (
      <View style={styles.container}>
        <FlatList horizontal={false} data={data} renderItem={this.renderItem} keyExtractor={(item, index) => `${index}`} />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  selectedItemChange: item => dispatch(selectedItemActions.selectedItemChange({ item })),
});

export default connect(
  null,
  mapDispatchToProps,
)(withRequests({ list: getListMovies })(MoviesList));
